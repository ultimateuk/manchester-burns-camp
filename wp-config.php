<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */



// ===================================================
// Load database info and local development parameters
// ===================================================
if ( file_exists( __DIR__ . '/local-config.php' ) ) {
    define('WP_LOCAL_DEV', true);

    // Include local settings
    include(__DIR__ . '/local-config.php');
} else {
    define('WP_LOCAL_DEV', false);

// ** MySQL settings - You can get this info from your web host ** //
    /** The name of the database for WordPress */
    define('DB_NAME', 'manchest_cbc');
    /** MySQL database username */
    define('DB_USER', 'manchest_cbcuser');
    /** MySQL database password */
    define('DB_PASSWORD', '9Tj^$55xt9');
    /** MySQL hostname */
    define('DB_HOST', 'localhost');
    define('WP_DEBUG', false);
}


/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{2,,d|.Tm5d0`BtEvya3-VwK@c-USS/ q+U69r zf3Jo-+|LW2n$tJm6%Hl-!SaG');
define('SECURE_AUTH_KEY',  'EK[{#A ;@sNV/@u|jb.Lb]V^@Cf!w:X4bD;/?)D?Bm|RHCUz--2+H]7Z@1X~Y3.T');
define('LOGGED_IN_KEY',    ']%jf_Paf-XpT-HyALr!3*$q[!=Cm j^FiJY/sct-W>_WX*chh|d<?<._8w{r+0Mt');
define('NONCE_KEY',        ':rAssg;;$oQi`uc0[ht8DTX8pNyB{FF_UUqzL$AtKr6z]^:2}z&S7%UfunRGH7#t');
define('AUTH_SALT',        '%4ur|o)-PI/ ;W5?T@_!q[2EH%Z<v,+y7+ b8oqB}0)0!JP84u)Nr^/`Z^5%cm:W');
define('SECURE_AUTH_SALT', 'Q8D#>@Pj16Qo_fhiAKL)}H}MF^floec@w)X-.K$mkVAW7Nici3vF-2+O=2GD6/p6');
define('LOGGED_IN_SALT',   '`*V)M@Pc1DK&]3X|omQt<W(Jicu7?`%zx0u] RH0{.Se`a`J/VXXp*wv}oae|cYt');
define('NONCE_SALT',       '$%Ma|2$@HN2|QD~6x`?!Xs<O9gg[#u2k>u>&Z6{2~=R%=B EDT`CMUl$d#.bR[XU');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'cbc';

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', __DIR__ . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
