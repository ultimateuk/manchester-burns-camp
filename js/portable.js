/* PRELOADER */
(function(a){var b=new Array,c=new Array,d=function(){},e=0;var f={splashVPos:"200px",loaderVPos:"250px",splashID:"#jpreContent",showSplash:true,showPercentage:true,debugMode:false,splashFunction:function(){}};var g=function(c){a(c).find("*:not(script)").each(function(){var c="";if(a(this).css("background-image").indexOf("none")==-1){c=a(this).css("background-image");if(c.indexOf("url")!=-1){var d=c.match(/url\((.*?)\)/);c=d[1].replace(/\"/g,"")}}else if(a(this).get(0).nodeName.toLowerCase()=="img"&&typeof a(this).attr("src")!="undefined"){c=a(this).attr("src")}if(c.length>0){b.push(c)}})};var h=function(){for(var a=0;a<b.length;a++){i(b[a])}};var i=function(b){var d=new Image;a(d).load(function(){j()}).error(function(){c.push(a(this).attr("src"));j()}).attr("src",b)};var j=function(){e++;var c=Math.round(e/b.length*100);a(jBar).stop().animate({width:c+"%"},500,"linear");if(f.showPercentage){a(jPer).text(c+"%")}if(e>=b.length){e=b.length;if(f.debugMode){var d=l()}k()}};var k=function(){a(jBar).stop().animate({width:"100%"},500,"linear",function(){a(jOverlay).fadeOut(800,function(){a(jOverlay).remove();d()})})};var l=function(){if(c.length>0){var a="ERROR - IMAGE FILES MISSING!!!\n\r";a+=c.length+" image files cound not be found. \n\r";a+="Please check your image paths and filenames:\n\r";for(var b=0;b<c.length;b++){a+="- "+c[b]+"\n\r"}return true}else{return false}};var m=function(b){jOverlay=a("<div></div>").attr("id","jpreOverlay").css({position:"fixed",top:0,left:0,width:"100%",height:"100%",zIndex:9999999}).appendTo("body");if(f.showSplash){jContent=a("<div></div>").attr("id","jpreSlide").appendTo(jOverlay);var c=a(window).width()-a(jContent).width();a(jContent).css({position:"absolute",top:f.splashVPos,left:Math.round(50/a(window).width()*c)+"%"});a(jContent).html(a(f.splashID).wrap("<div/>").parent().html());a(f.splashID).remove();f.splashFunction()}jLoader=a("<div></div>").attr("id","jpreLoader").appendTo(jOverlay);var d=a(window).width()-a(jLoader).width();a(jLoader).css({position:"absolute",top:f.loaderVPos,left:Math.round(50/a(window).width()*d)+"%"});jBar=a("<div></div>").attr("id","jpreBar").css({width:"0%",height:"100%"}).appendTo(jLoader);if(f.showPercentage){jPer=a("<div></div>").attr("id","jprePercentage").css({position:"relative",height:"100%"}).appendTo(jLoader).html("Loading...")}};a.fn.jpreLoader=function(b,c){if(b){a.extend(f,b)}if(typeof c=="function"){d=c}m(this);g(this);h();return this}})(jQuery);

$('#preSplash').show();

// Preloader
$('#preSplash p').fadeIn(300, function(){
	$('body').jpreLoader({
		splashID: "#preSplash"
	}, function() {
		/* STARTUP EVENTS */
    		
    		$('body').css('overflow', 'visible');

			// Reset browser position on reload
			$('html, body').stop().scrollLeft(0);

		/* END STARTUP */
	});
});

//Prevent links closing popups
$('.paper a').click(function(ev){
	ev.stopPropagation();
	if($(this).prop('id') == 'volunteer-right') {
		var index = $('#volunteer-content div.current').index()+2;
		$('#volunteer-content div.current').removeClass('current').fadeOut(function(){
			var num_pages = $('#volunteer-content > *').length;
			if(index > num_pages) index = 2;
			$('#volunteer-content div:nth-child('+index+')').fadeIn().addClass('current');
		});
	// Volunteer page previous
	}else if($(this).prop('id') == 'volunteer-left') {
		var index = $('#volunteer-content div.current').index();
		$('#volunteer-content div.current').removeClass('current').fadeOut(function(){
			var num_pages = $('#volunteer-content > *').length;
			if(index <= 1) index = num_pages;
			$('#volunteer-content div:nth-child('+index+')').fadeIn().addClass('current');
		});
	}
});