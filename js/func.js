/* PRELOADER */
(function(a){var b=new Array,c=new Array,d=function(){},e=0;var f={splashVPos:"200px",loaderVPos:"250px",splashID:"#jpreContent",showSplash:true,showPercentage:true,debugMode:false,splashFunction:function(){}};var g=function(c){a(c).find("*:not(script)").each(function(){var c="";if(a(this).css("background-image").indexOf("none")==-1){c=a(this).css("background-image");if(c.indexOf("url")!=-1){var d=c.match(/url\((.*?)\)/);c=d[1].replace(/\"/g,"")}}else if(a(this).get(0).nodeName.toLowerCase()=="img"&&typeof a(this).attr("src")!="undefined"){c=a(this).attr("src")}if(c.length>0){b.push(c)}})};var h=function(){for(var a=0;a<b.length;a++){i(b[a])}};var i=function(b){var d=new Image;a(d).load(function(){j()}).error(function(){c.push(a(this).attr("src"));j()}).attr("src",b)};var j=function(){e++;var c=Math.round(e/b.length*100);a(jBar).stop().animate({width:c+"%"},500,"linear");if(f.showPercentage){a(jPer).text(c+"%")}if(e>=b.length){e=b.length;if(f.debugMode){var d=l()}k()}};var k=function(){a(jBar).stop().animate({width:"100%"},500,"linear",function(){a(jOverlay).fadeOut(800,function(){a(jOverlay).remove();d()})})};var l=function(){if(c.length>0){var a="ERROR - IMAGE FILES MISSING!!!\n\r";a+=c.length+" image files cound not be found. \n\r";a+="Please check your image paths and filenames:\n\r";for(var b=0;b<c.length;b++){a+="- "+c[b]+"\n\r"}return true}else{return false}};var m=function(b){jOverlay=a("<div></div>").attr("id","jpreOverlay").css({position:"fixed",top:0,left:0,width:"100%",height:"100%",zIndex:9999999}).appendTo("body");if(f.showSplash){jContent=a("<div></div>").attr("id","jpreSlide").appendTo(jOverlay);var c=a(window).width()-a(jContent).width();a(jContent).css({position:"absolute",top:f.splashVPos,left:Math.round(50/a(window).width()*c)+"%"});a(jContent).html(a(f.splashID).wrap("<div/>").parent().html());a(f.splashID).remove();f.splashFunction()}jLoader=a("<div></div>").attr("id","jpreLoader").appendTo(jOverlay);var d=a(window).width()-a(jLoader).width();a(jLoader).css({position:"absolute",top:f.loaderVPos,left:Math.round(50/a(window).width()*d)+"%"});jBar=a("<div></div>").attr("id","jpreBar").css({width:"0%",height:"100%"}).appendTo(jLoader);if(f.showPercentage){jPer=a("<div></div>").attr("id","jprePercentage").css({position:"relative",height:"100%"}).appendTo(jLoader).html("Loading...")}};a.fn.jpreLoader=function(b,c){if(b){a.extend(f,b)}if(typeof c=="function"){d=c}m(this);g(this);h();return this}})(jQuery)

var balloons_down = true;
var balloon_timer;
var is_scrolling = false;
var moon_active = false;
var nav_status = 'down';
var fly_plane = true;
var plane_flying = false;
var plane_timer;
$('#preSplash').show();

/*
 *	ON PAGE READY
 */
$(document).ready(function (){



	// Preloader
	$('#preSplash p').fadeIn(300, function(){
		$('body').jpreLoader({
			splashID: "#preSplash"
		}, function() {
			/* STARTUP EVENTS */

	    		$('body').css('overflow', 'visible');

				// Reset browser position on reload
				$('html, body').stop().scrollLeft(0);

				// Raise nav after 2 secs
				setTimeout(function(){ $('#nav').trigger('mouseleave'); }, 2000);

				// Raise 'about us' after 4 sec
				balloon_timer = setTimeout(function(){ slideBalloons(1); }, 4000);

				// Start parallax
				var sp, pp;
				if($('html').hasClass('csstransforms')){sp=pp="transform";}else{sp="scroll";pp="position";}
				$.stellar({verticalScrolling: false, scrollPosition: sp});

				// Fly the plane
				flyPlane();

				// Start flashing sign
				neonFlash(1);

			/* END STARTUP */
		});
	});

	// Balloons & about us
	$('img.balloons').on('click', function(ev){ ev.stopPropagation(); $('.paper').each(function(){ hideContent($(this)); }); slideBalloons(); });
	$('div.balloons').on('click', function(){ slideBalloons(0); });
		wobbleBalloons();

	// Signs and content
	$('.sign, img.balloons, .sign-link').one('click', function(){ clearTimeout(balloon_timer); });
	$('body').on('click', '.sign, .sign-link', function(ev){
		ev.preventDefault();
		if( !balloons_down ) slideBalloons(0);
		target_id = '#text-'+$(this).prop('id').substr(5);
		$('.paper:not('+target_id+')').each(function(){ hideContent($(this)); });
		showContent($(target_id));
	});
	$('.paper:not(#text-contact, #text-events)').on('click', function(){ hideContent($(this)); });

	// On screen scroll
	$(window).scroll(function () {
		// Move moon at set point
		var win_x = $(window).scrollLeft();
		if(win_x > 600 && win_x < 2600 && !moon_active) {
			var the_moon = $('#moon');
			$('#moon').css('bottom', (800-(win_x*0.3))+'px');
		}
		// Show plane in front of tree / behind paper
		if(($('#plne-help').position().left+win_x) > 5250){
			$('#plne-help').css('z-index', 201);
		}else{
			$('#plne-help').css('z-index', 700);
		}
    });

	// Pause/resume plane flying on hover
	$('#plne-help').on({
		mouseenter: function(){ $(this).stop(); },
		mouseleave: function(){ flyPlane(1); }
	})

 	// Show donation text
	$('#help-nav a').on('click', function(ev){
		ev.stopPropagation();
		// Check if already selected
		if(!$(this).hasClass('selected')){
			ev.preventDefault();
			$('.help-t').hide();
			$('#help-nav a').removeClass('selected');
			$(this).addClass('selected');
			$('#'+$(this).prop('id')+'-t').fadeIn(200);
		}
	});

	// Show nav on mouseover
	$('#nav').on({
		mouseenter: function(){
			if(nav_status == 'up' && !is_scrolling){
				nav_status = 'moving';
				$(this).animate({top: 0}, function(){ nav_status = 'down'; });
			}
		},
		mouseleave: function(){
			if(nav_status == 'down'){
				nav_status = 'moving';
				$(this).animate({top: -240}, function(){ nav_status = 'up'; });
			}
		}
	});

	// Open phonebox on click
	$('#phonebox').click(function(){
		var $pb = $('#text-contact');
		if($pb.css('display') == 'none') showContent($pb);
	});

	//Prevent links closing popups
	$('.paper a').not($('#text-events a')).click(function(ev){
		ev.stopPropagation();
		if($(this).prop('href').indexOf("#") >= 0) ev.preventDefault();

		// Donation page change
		if($(this).hasClass('help-page')) {
			if($('#help-p1').css('display') == 'none') {
				$('#help-p2').fadeOut(function(){ $('#help-p1').fadeIn(); });
			}else{
				$('#help-p1').fadeOut(function(){ $('#help-p2').fadeIn(); });
			}
		// Volunteer page next
		}else if($(this).prop('id') == 'volunteer-right') {
			var index = $('#volunteer-content div.current').index()+2;
			$('#volunteer-content div.current').removeClass('current').fadeOut(function(){
				var num_pages = $('#volunteer-content > *').length;
				if(index > num_pages) index = 2;
				$('#volunteer-content div:nth-child('+index+')').fadeIn().addClass('current');
			});
		// Volunteer page previous
		}else if($(this).prop('id') == 'volunteer-left') {
			var index = $('#volunteer-content div.current').index();
			$('#volunteer-content div.current').removeClass('current').fadeOut(function(){
				var num_pages = $('#volunteer-content > *').length;
				if(index <= 1) index = num_pages;
				$('#volunteer-content div:nth-child('+index+')').fadeIn().addClass('current');
			});
		}
	});
});

// Delay function
var delay = (function(){
	var timer = 0;
	return function(callback, ms){
		clearTimeout (timer);
		timer = setTimeout(callback, ms);
	};
})();

// Hide the content
function hideContent($obj) {
	if(!is_scrolling){
		if($obj.prop('id') == 'text-aboutus'){
			slideBalloons(0);
		}else if($obj.prop('id') == 'text-help'){
			$obj.animate({bottom: -600}, 1000);
			// Enable plane animation
			fly_plane = true;
			if(!plane_flying) flyPlane();
		}else{
			$obj.fadeOut();
		}
	}
}

// Show the content
function showContent($obj) {
	$('#nav').trigger('mouseleave');
	if(!is_scrolling){
		if($obj.prop('id') == 'text-aboutus'){
			slideBalloons(1);
		}else if($obj.prop('id') == 'text-help'){
			$obj.animate({bottom: 90}, 1000);
			// Disable plane animation
			fly_plane = false;
		}else if($obj.css('display') == 'none'){
			$obj.fadeIn();
		}
		if($obj.prop('id') == 'text-contact'){
			scrollTo(1750,'');
		}else{
			$obj.scrollViewburns('', -280);
		}
	}
}

// Move air balloons around slightly
function wobbleBalloons(){
	$('img.balloons').animate({top: '0px', left: '10px'}, 1000)
		.animate({top: '+=2px', left: '-=4px'})
		.animate({top: '+=2px', left: '+=2px'})
		.animate({top: '+=4px', left: '+=6px'}, 1000)
		.animate({top: '-=1px', left: '+=2px'})
		.animate({top: '-=2px', left: '+=1px'}, function(){ wobbleBalloons() });
}

// Slide up air balloons (+ show 'About us')
function slideBalloons(up){
	if(up == undefined) {
		if(balloons_down) up = true;
		else up = false;
	}
	// Slide up
	if(up && balloons_down){
		balloons_down = false;
		$('#balloons-box').animate({bottom: '+=550'}, 1000);
	// Slide down
	}else if(!up && !balloons_down){
		balloons_down = true;
		$('#balloons-box').animate({bottom: '-=550'}, 1000);
	}
}

// Animate plane
function flyPlane(force_fly){
	if(force_fly == undefined) force_fly = 0;
	if(fly_plane || force_fly){
		clearTimeout(plane_timer);
		distance = $(window).width()+200;
		speed = (distance-($('#plne-help').offset().left-$(window).scrollLeft()))*6;
		if(speed > (distance*6)) speed = distance*7;
		plane_flying = true;
		$('#plne-help').animate({left: $(window).width()}, speed, function(){ plane_flying = false; $('#plne-help').css('left', -650); plane_timer = setTimeout(function(){flyPlane();}, 4000); });
	}
}

// Animate up neon sign
function neonFlash(i){
	if(i < 3) {
		$('#sign-contact').css('background-position', -(200*i)+'px 0');
		++i;
	} else {
		$('#sign-contact').css('background-position', '0 0');
		i = 1;
	}
	setTimeout('neonFlash('+i+')', 400);
}

// Window scroll function
function scrollTo(to, speed){
	if(!is_scrolling){
		is_scrolling = true;
		$('html, body').animate({
			scrollLeft: to
		}, Math.abs($(window).scrollLeft()-to)*2, function(){
			is_scrolling = false;
		});
	}
}

$.fn.scrollViewburns = function (speed, offset) {
	if(speed == undefined) speed = $(window).scrollLeft();
	if(offset == undefined) offset = 0;
	return this.each(function () {
		var distance = $(this).offset().left+offset;
		scrollTo(distance, speed);
	});
}