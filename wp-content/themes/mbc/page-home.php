<?php
/**
 * Template Name: Home Page
 */

?>
<?php
$mobile_device = (bool)preg_match(
    '#\b(ip(hone|od|ad)|android|opera m(ob|in)i|windows (phone|ce)|blackberry|tablet' .
    '|s(ymbian|eries60|amsung)|p(laybook|alm|rofile/midp|laystation portable)|nokia|fennec|htc[\-_]' .
    '|mobile|up\.browser|[1-4][0-9]{2}x[1-4][0-9]{2})\b#i',
    $_SERVER['HTTP_USER_AGENT']
);
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html class="ie ie6 no-js" dir="ltr" lang="en-US"> <![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7 no-js" dir="ltr" lang="en-US"> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8 no-js" dir="ltr" lang="en-US"> <![endif]-->
<!--[if IE 9 ]>
<html class="ie ie9 no-js" dir="ltr" lang="en-US"> <![endif]-->
<html class="no-js" dir="ltr" lang="en-US"><!--<![endif]-->
<head profile="https://gmpg.org/xfn/11">
    <title>Manchester Children's Burns Camp</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width; initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/styles.css"/>
    <link href="css/lightbox.css" rel="stylesheet"/>

    <!--[if gt IE 8]><!-->
    <link href='https://fonts.googleapis.com/css?family=Quicksand:400,700|Amatic+SC:700' rel='stylesheet'
          type='text/css'><!--<![endif]-->
    <script src="js/modernizr.custom.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/lightbox.js"></script>

</head>
<body class="scrollable horizontal<?php
if ($mobile_device) { ?> mobile no-js<?php
} ?>">
<noscript>
    <div id="no-js-warn">
        <strong>Important: </strong>Your Javascript is disabled/unavailable. Certain features on this website may not
        function as intended unless your Javascript is enabled.
    </div>
</noscript>
<div id="preSplash">
    <p>Setting up camp...</p>
</div>
<div id="page-contents">
    <div id="mountains-bg" data-stellar-background-ratio="0.3"></div>
    <div id="mountains-fg" data-stellar-background-ratio="0.5"></div>
    <div id="night-bg"></div>
    <img id="plne-help" class="sign-link" src="img/plane.png" alt="Plane"/>

    <div id="night">
        <img data-stellar-ratio="1.7" id="moon" src="img/moon.png" alt="Moon"/>
    </div>

    <nav id="nav">
        <ul>
            <li><a class="sign-link" id="link-aboutus" href="#">About us</a></li>
            <li><a class="sign-link" id="link-whatwedo" href="#">What we do...</a></li>
            <li><a class="sign-link" id="link-events" href="#">Our Events</a></li>
            <li><a class="sign-link" id="link-whowe" href="#">Who we do it for...</a></li>
            <li><a class="sign-link" id="link-contact" href="#">Contact us</a></li>
            <li><a class="sign-link" id="link-volunteers" href="#">Our volunteers</a></li>
            <li><a class="sign-link" id="link-help" href="#">Donate</a></li>
        </ul>
        <div id="logo-main"><img src="img/logo-main.png" alt="Manchester Children's Burns Camp"/></div>
    </nav>
    <div data-stellar-ratio="0.7" id="lake"></div>
    <img id="sun" src="img/sun.png" alt="Sun"/>
    <div id="balloons-box">
        <img class="balloons" src="img/balloons.png" alt="Balloons"/>
        <div id="text-aboutus" class="balloons content">
            <h2>About us...</h2>
            <p><strong>Background</strong><br/>
                The Manchester Children’s Burns Camp began nearly 20 years ago, when a small
                number of burn injured children enjoyed a weekend away. Now, we run two annual,
                week-long Camps for up to 50 children and young people as well as developing a
                training programme for young people who have attended Camp in the past to learn
                how to become Camp Leaders in the future.</p>
            <p><strong>Activities</strong><br/>
                We also organise several day activities – such as indoor skiing and tobogganing -
                so that those who have experienced burn injuries can enjoy physical challenges
                alongside their peers. We also participate in an International Exchange Programme
                with a Burns Camp in Colorado, USA, to share ideas and improve what we do.</p>
            <p><strong>Free Places</strong><br/>
                The Camps and training programmes are free for invited children and young
                people to attend. The costs are funded solely from charitable donations.</p>
        </div>
    </div>


    <div id="text-events" class="paper content">
        <h2 class="events-title">Upcoming Events</h2>
        <div>

            <?php
            $date = '20' . date('ymd');
            $postcount = 0;

            query_posts(array(
                'post_type' => 'services',
                'order'     => 'ASC',
                'orderby'   => 'meta_value',
                'meta_key'  => 'date-field'
            ));

            $args = array('post_type'      => 'events',
                          'posts_per_page' => 50,
                          'order'          => 'DSC',
                          'orderby'        => 'meta_value',
                          'meta_key'       => 'date'
            );
            $loop = new WP_Query($args);
            while ($loop->have_posts()) : $loop->the_post();
                if (!$postcount) {
                    $postcount = 0;
                }
                $postcount++;

                if (get_field('date') < $date) {
                    echo '<div class="event-content past">';
                    ?>
                    <div class="event-date">
                        <?php
                        $eventdate = get_field('date');
                        $eventdate2 = strtotime($eventdate);
                        echo $eventdate3 = date('jS F Y', $eventdate2);
                        ?>
                    </div>
                    <?php
                    echo '<h2 class="event-title">';
                    the_title();
                    echo '</h2>';
                    ?>

                    <!--<a class="gallery-link" href="img/image-3.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward.">VIEW GALLERY IMAGES</a>
                    <a class="gallery-link" href="img/image-3.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."></a>-->
                    <div class="event-content">
                        <?php
                        echo get_field('short_description');
                        ?>
                    </div>
                    <?php
                    //var_dump (get_field(images));

                    $imagerows = (get_field('images'));
                    if (!empty($imagerows[0]['photo'])) {
                        ?><a class="gallery-link" data-lightbox="example-set<?php
                        echo $postcount ?>"
                             href="<?php
                             echo $imagerows['0']['photo']; ?>">VIEW GALLERY IMAGES</a>
                        <?php
                        //var_dump ($imagerows);
                        $counter = 0;
                        foreach ($imagerows as $imagerow) {
                            if ($counter++ < 2) {
                                continue;
                            }
                            ?>
                            <a data-lightbox="example-set<?php
                            echo $postcount ?>" href="<?php
                            echo $imagerow['photo'] ?>"></a>
                            <?php
                        }
                    }

                    echo '</div>';
                } else {
                    echo '<div class="event-content upcoming">';
                    ?>
                    <div class="event-date">
                        <?php
                        $eventdate = get_field('date');
                        $eventdate2 = strtotime($eventdate);
                        echo $eventdate3 = date('jS F Y', $eventdate2);
                        ?>
                    </div>
                    <?php
                    echo '<h2 class="event-title"> ';
                    the_title();
                    echo '</h2>';
                    ?>
                    <div class="event-content">
                        <?php
                        echo get_field('short_description');
                        ?>
                    </div>
                    <?php
                    echo '</div>';
                    ?>
                    <?php
                }
            endwhile
            ?>
        </div>
        <div style="display:inline-block; font-size: 18px; bottom: 40px; left: 30px; position: absolute;"><a
                    class="pastevents">PAST EVENTS</a></div>
        <div style="float:right; display:inline-block; font-size: 18px; bottom: 40px; right: 20px; position: absolute;">
            <a class="upcomingevents">FUTURE EVENTS</a></div>
    </div>

    <script>
        $(document).ready(function () {

            let i;
            let first = 0;
            let last = 0;

            const segmentspast = $(".past");
            for (i = 0; i < segmentspast.length; i += 2) {
                segmentspast.slice(i, i + 2).wrapAll("<div class='pastwrap'></div>");
            }

            const segmentsfuture = $(".upcoming");
            for (i = 0; i < segmentsfuture.length; i += 2) {
                segmentsfuture.slice(i, i + 2).wrapAll("<div class='upcomingwrap'></div>");
            }

            if ($(".upcomingwrap").length) {
                $(".upcomingwrap").last().addClass("activeevent").show();
            } else {
                $(".pastwrap").first().addClass("activeevent").show();
                $(".events-title").html("Past events");
            }

            $(".pastevents").click(function (e) {
                e.preventDefault();

                if ($(".activeevent").is(':last-child')) {
                    last = 1;
                } else {
                    last = 0;
                }
                if (last == 0) {
                    $(".activeevent").hide();
                    $(".activeevent").removeClass("activeevent").next().addClass("activeevent").show()
                }
                if ($(".activeevent").hasClass("pastwrap")) {
                    $(".events-title").html("Past events");
                }

            })

            $(".upcomingevents").click(function (e) {
                e.preventDefault();
                if ($(".activeevent").is(':first-child')) {
                    first = 1;
                } else {
                    first = 0;
                }
                if (first == 0) {
                    $(".activeevent").hide();
                    $(".activeevent").removeClass("activeevent").prev().addClass("activeevent").show()
                }
                if ($(".activeevent").hasClass("upcomingwrap")) {
                    $(".events-title").html("Upcoming events");
                }
            })


            /*   size_div = $(".event-content").size();
               x=3;
               y=0;
               console.log(y);

           $('.event-content:lt('+x+')').show();



           $(".upcomingevents").click(function(e){
            x =(x+3 <= size_div) ? x+3 : size_div;
            y =(y+6 <= size_div) ? y+3 : size_div - 3;
               $('.event-content').hide();
               $('.event-content:lt('+x+')').not('.event-content:lt('+y+')').show();
           e.preventDefault();
       });
           $(".pastevents").click(function(e){
               console.log("what");
               x =(x-3 <= 0) ? 3 : x-3;
               y =(y-3 <= 0) ? 0 : y-3;
               $('.event-content').hide();
               $('.event-content:lt('+x+')').not('.event-content:lt('+y+')').show();
               e.preventDefault();
           });

*/

        });


    </script>


    <div id="text-whatwedo" class="paper content">
        <h2>What we do</h2>
        <p>Each year, around 750 <strong>burn injured children</strong> and young people are treated under the care of
            the Manchester Paediatric Burns Service. Those aged between 4 and 16 are invited to attend the appropriate
            camps and activities.</p>
        <p><strong>The Team</strong><br/>We are a team of specially trained <strong>volunteers</strong> who come from a
            variety of different health service professions (such as doctors, nurses, clinical psychologists, play
            specialists, occupational therapists and physiotherapists), along with fire fighters and adult burn
            survivors.</p>
        <p><strong>The Activities</strong><br/>The camps are carefully designed to help children and young people face
            the challenges of living with a burn injury. These may include:</p>
        <ul>
            <li><strong>Physically challenging activities</strong> such as rock climbing, abseiling and caving to build
                self-confidence in what their bodies can still achieve.
            </li>
            <li><strong>Day trips</strong> such as bowling and swimming which allow children to face the public with the
                support of each other.
            </li>
            <li><strong>Teamwork</strong> challenges to help improve social and communication skills.</li>
            <li>Small group activities to learn effective <strong>coping strategies</strong> for dealing with difficult
                situations such as teasing or bullying.
            </li>
        </ul>
    </div>
    <div id="text-volunteers" class="paper content">
        <a id="volunteer-left"></a>
        <div id="volunteer-content">
            <h2>Our Volunteers</h2>
            <div class="current">
                <img src="img/volunteer-frame.png" class="volunteer-img"
                     style="background-image: url('img/volunteer-ian-melville.jpg');" alt="Ian Melville"/>
                <p><span>Name: Ian Melville</span></p>
                <p><span>Age: 45</span></p>
                <p><span>From: Manchester</span></p>
                <p>Ian has been an operational Fire Fighter for 18 years, currently serving as a Watch Manager at Moss
                    Side, Manchester.</p>
            </div>

            <div style="display: none;">
                <img src="img/volunteer-frame.png" class="volunteer-img"
                     style="background-image: url('img/volunteer-pete.png');" alt="Sarah"/>
                <p><span>Name: Pete Fitzpatrick</span></p>
                <p><span>From: Manchester</span></p><br>
                <p>I am currently a crew manager at Manchester Central Fire Station and have worked for the fire service
                    for 21 years; I first got involved with Young Adults Burns camp several years ago</p>
            </div>

            <div style="display: none;">
                <img src="img/volunteer-frame.png" class="volunteer-img"
                     style="background-image: url('img/volunteer-katrina.png');" alt="Sarah"/>
                <p><span>Name: Katrina Keating</span></p>
                <p><span>From: Manchester</span></p><br>
                <p>Katrina is Burns Research Nurse at the Royal Manchester Children's Hospital. She has been a camp
                    leader since 2010 and now leads the younger children's camp.</p>
            </div>

            <div style="display: none;">
                <img src="img/volunteer-frame.png" class="volunteer-img"
                     style="background-image: url('img/volunteer-halima.png');" alt="Sarah"/>
                <p><span>Name: Halima Zubaid</span></p>
                <p><span>From: Manchester</span></p><br>
                <p>Halima is a staff Nurse on the Burns Unit at the Children's Hospital. I've done Burns Camp twice and
                    loved every minute of it! I also help on day activities.</p>
            </div>

            <div style="display: none;">
                <img src="img/volunteer-frame.png" class="volunteer-img"
                     style="background-image: url('img/volunteer-tom.png');" alt="Sarah"/>
                <p><span>Name: Tom Gannon</span></p>
                <p><span>From: Manchester</span></p><br>
                <p>Thomas is a volunteer who is also a burns survivor; having suffered a 40% burn injury when he was
                    just 6 years old. He is keen to support others who face similar challenges and believes attending
                    burns camp as a child himself helped him to build the confidence to thrive as a burns survivor.</p>
            </div>

        </div>
        <a id="volunteer-right"></a>
    </div>
    <div id="text-help" class="paper content">
        <h2>How you can help...</h2>
        <div id="help-content">
            <div id="help-p1">
                <p class="large-text"><a class="help-page" href="#">Next Page &gt;</a></p>
                <p>It costs around £500 for a child to attend one of our residential Camps. All the camp
                    staff give their time voluntarily so we only pay for the facilities, equipment and for
                    qualified instructors to ensure safety at all times.</p>
                <p><strong>Personal Donations</strong></p>
                <p>We depend on charity donations – whether from a business, fundraising or a
                    personal contribution. Children who attend are not asked to contribute financially; but
                    a lot of the families who know how amazing our camps are have held a fundraising
                    event to say ‘thank you’ and ensure that places at the camp are available for others in
                    the future.</p>
                <p><strong>Business Support</strong></p>
                <p>If you’re looking for a local charity to support where you can see directly where
                    the money has gone, we are ideal. You know that 100% of your donation directly
                    provides a life-changing experience.</p>
                <p>Each year we have a special Visitors Day at the Camps so you can come along and
                    see for yourself how your donation has been used.</p>
            </div>
            <div id="help-p2">
                <p class="large-text"><a class="help-page" href="#">&lt; Previous Page</a></p>
                <p><strong>Download a copy of our Annual Reports</strong></p>
                <p class="left downloads">
                    <strong>
                        <a download="MCBC_Annual_Report_2016.pdf" href="/files/annual-report-2016.pdf">2016</a>&nbsp;&bull;
                        <a download="MCBC_Annual_Report_2017.pdf" href="/files/MCBC_Annual_Report_2017.pdf">2017</a>&nbsp;&bull;
                        <a download="MCBC_Annual_Report_2018.pdf" href="/files/MCBC_Annual_Report_2018.pdf">2018</a>
                    </strong>
                </p>
                <div style="clear: both;"></div>
                <div id="help-nav">
                    <a target="_BLANK" href="https://www.justgiving.com/mcbf/Donate/" id="help-jg"
                       class="selected just-giving"></a>
                    <div style="clear: both;"></div>
                    <!--                    <p><strong>Click on a logo for instructions of how to donate.</strong></p>-->

                </div>
                <div class="help-t">
                    Donations can also be made through Just Giving. Search for Manchester University Hospitals NHS
                    Foundation Trust, use <strong>‘Edit My Page’</strong> to <strong>SPECIFY</strong> that your donation
                    is for <strong>MANCHESTER CHILDREN’S BURNS CAMP</strong> charity number <strong>R000278</strong>
                    <br><br>
                    <strong>By Post:</strong><br>
                    Cheques should be made payable to Manchester Children’s Burns Camp and sent to:<br><br>
                    <span class="right">Alison Thomlinson<br>
                    Burns Camp Co coordinator<br>
                    Burns Service (Ward 81)<br>
                    Royal Manchester Children’s Hospital<br>
                    Oxford Road<br>
                    Manchester<br>
                    M13 9WL</span>
                </div>
            </div>
        </div>
    </div>
    <img data-stellar-ratio="1.4" src="img/hotairballoon.png" id="sign-events" class="sign" alt="Events"/>
    <img data-stellar-ratio="1.4" src="img/sign-whatwedo.png" id="sign-whatwedo" class="sign" alt="What we do"/>
    <img data-stellar-ratio="1.4" src="img/sign-whowe.png" id="sign-whowe" class="sign" alt="Who we do it for"/>
    <img data-stellar-ratio="1.3" src="img/big-tree.png" id="big-tree" alt="Big Tree"/>
    <img data-stellar-ratio="1.1" src="img/log-n-lantern.png" id="log-lantern" alt="Log &amp; Lantern"/>
    <img data-stellar-ratio="1.2" src="img/cabin.png" id="cabin" alt="Log Cabin"/>
    <div data-stellar-ratio="1.1" id="phonebox">
        <div id="text-contact" class="paper">
            <div class="contact-right contact-right-2">
                <p>
                    <strong><span>Tel:</span> 0161 701 8142<br/>
                        <span>Mobile:</span> 07827 848240<br/>
                        <!--<span>Enq:</span>--><br/> <a href="mailto:Alison.Thomlinson@mft.nhs.uk">Alison.Thomlinson@mft.nhs.uk</a></strong><br/><br/>
                    Children's Burns Camp is part of Manchester University NHS Foundation Trust Charity (Registered
                    Charity Number 1049274).
                </p>
            </div>
            <div class="contact-right">
                <p>
                    <strong>Manchester Children's<br/>
                        Burns Camp<br/>
                        Burns Service<br/>
                        Royal Manchester<br/>
                        Children's Hospital<br/>
                        Oxford Road<br/>
                        Manchester<br/>
                        M13 9WL</strong>
                </p>
            </div>
        </div>
    </div>
    <div data-stellar-ratio="1.1" id="sign-contact" class="sign"></div>
    <img data-stellar-ratio="0.7" src="img/hikers.png" id="hikers" alt="Hikers"/>
    <img data-stellar-ratio="1.3" src="img/horses.png" id="horses" alt="Horses"/>
    <img data-stellar-ratio="1.1" src="img/sign-volunteers.png" id="sign-volunteers" class="sign" alt="Our volunteers"/>
    <img data-stellar-ratio="1.4" src="img/sign-help.png" id="sign-help" class="sign" alt="How you can help"/>

    <img data-stellar-ratio="1.1" src="img/birds.png" id="birds" alt="Birds"/>

    <div id="mountain-lake"></div>
    <div id="climber"></div>
    <div id="text-whowe" class="paper content">
        <h2>Who we do it for...</h2>
        <p><strong>Recovering From A Burn Injury</strong><br/>
            Following a traumatic injury, children and young people can face many physical and
            psychological challenges. The <strong>process of recovery</strong> can take a long time – arduous,
            painful dressing changes and repeated trips to the hospital for further surgery are
            combined with regular massage regimes and wearing specialist tight-fitting garments
            for up to three years to help minimise scarring. They also have to adapt to their bodies
            now looking different, learning to reintegrate into social and family life in a society
            that often discriminates against disfigured people.</p>
        <p><strong>How Burns Camps Help</strong><br/>
            At Camp, they meet and socialise with young people just like themselves, as well as
            meeting adult burn survivors and members of the Burn Care Team away from clinical
            procedures.</p>
        <p>So whether they are <strong>climbing</strong> a cliff face, <strong>canoeing</strong> across a lake, solving
            a <strong>team
                challenge</strong>, going <strong>swimming</strong> for the first time since their injury or <strong>chatting</strong>
            late into
            the night, these young people gain far more than a fun summer holiday. They gain
            confidence and new skills to help them in the years ahead; they gain <strong>friendship and
                support</strong> with others who totally understand.</p>
    </div>
</div>
<div id="sky"></div>

<div id="grass" data-stellar-background-ratio="1.5"></div>
<script type="text/javascript" src="js/stellar.min.js"></script>

<?php
if (!$mobile_device) { ?>
    <script type="text/javascript" src="js/func.js"></script>
<?php
}else{ ?>
    <script type="text/javascript" src="js/portable.js"></script>
<?php
} ?>


</body>
</html>
