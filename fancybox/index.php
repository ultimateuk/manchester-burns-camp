<?php
$mobile_device=(bool)preg_match('#\b(ip(hone|od|ad)|android|opera m(ob|in)i|windows (phone|ce)|blackberry|tablet'.
                    '|s(ymbian|eries60|amsung)|p(laybook|alm|rofile/midp|laystation portable)|nokia|fennec|htc[\-_]'.
                    '|mobile|up\.browser|[1-4][0-9]{2}x[1-4][0-9]{2})\b#i', $_SERVER['HTTP_USER_AGENT'] );
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie ie6 no-js" dir="ltr" lang="en-US"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 no-js" dir="ltr" lang="en-US"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 no-js" dir="ltr" lang="en-US"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 no-js" dir="ltr" lang="en-US"> <![endif]-->
<html class="no-js" dir="ltr" lang="en-US"><!--<![endif]-->
<head profile="http://gmpg.org/xfn/11">
	<title>Manchester Children's Burns Camp</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width; initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/styles.css" />
	<!--[if gt IE 8]><!--><link href='http://fonts.googleapis.com/css?family=Quicksand:400,700|Amatic+SC:700' rel='stylesheet' type='text/css'><!--<![endif]-->
	<script src="js/modernizr.custom.js"></script>
	<script type="text/javascript" src="js/jquery.min.js"></script>
</head>
<body class="scrollable horizontal<?php if($mobile_device){ ?> mobile no-js<?php } ?>">
<noscript>
	<div id="no-js-warn">
		<strong>Important: </strong>Your Javascript is disabled/unavailable. Certain features on this website may not function as intended unless your Javascript is enabled.
	</div>
</noscript>
<div id="preSplash">
	<p>Setting up camp...</p>
</div>
<div id="page-contents">
	<div id="mountains-bg" data-stellar-background-ratio="0.3"></div>
	<div id="mountains-fg" data-stellar-background-ratio="0.5"></div>
	<div id="night-bg"></div>
	<img id="plne-help" class="sign-link" src="img/plane.png" alt="Plane" />

	<div id="night">
		<img data-stellar-ratio="1.7" id="moon" src="img/moon.png" alt="Moon" />
	</div>

	<nav id="nav">
		<ul>
			<li><a class="sign-link" id="link-aboutus" href="#">About us</a></li>
			<li><a class="sign-link" id="link-whatwedo" href="#">What we do...</a></li>
			<li><a class="sign-link" id="link-whowe" href="#">Who we do it for...</a></li>
			<li><a class="sign-link" id="link-contact" href="#">Contact us</a></li>
			<li><a class="sign-link" id="link-volunteers" href="#">Our volunteers</a></li>
			<li><a class="sign-link" id="link-help" href="#">Donate</a></li>
		</ul>
		<div id="logo-main"><img src="img/logo-main.png" alt="Manchester Children's Burns Camp" /></div>
	</nav>
	<div data-stellar-ratio="0.7" id="lake"></div>
	<img id="sun" src="img/sun.png" alt="Sun" />
	<div id="balloons-box">
		<img class="balloons" src="img/balloons.png" alt="Balloons" />
		<div id="text-aboutus" class="balloons content">
			<h2>About us...</h2>
			<p><strong>Background</strong><br/>
The Manchester Children’s Burns Camp began nearly 20 years ago, when a small
number of burn injured children enjoyed a weekend away. Now, we run two annual,
week-long Camps for up to 50 children and young people as well as developing a
training programme for young people who have attended Camp in the past to learn
how to become Camp Leaders in the future.</p>
<p><strong>Activities</strong><br/>
We also organise several day activities – such as indoor skiing and tobogganing -
so that those who have experienced burn injuries can enjoy physical challenges
alongside their peers. We also participate in an International Exchange Programme
with a Burns Camp in Colorado, USA, to share ideas and improve what we do.</p>
<p><strong>Free Places</strong><br/>
The Camps and training programmes are free for invited children and young
people to attend. The costs are funded solely from charitable donations.</p>
		</div>
	</div>

    <div id="text-events" class="paper content">
        <h2>Events</h2>
        <p>Each year, around 750 <strong>burn injured children</strong> and young people are treated under the care of the Manchester Paediatric Burns Service. Those aged between 4 and 16 are invited to attend the appropriate camps and activities.</p>
        <p><strong>The Team</strong><br/>We are a team of specially trained <strong>volunteers</strong> who come from a variety of different health service professions (such as doctors, nurses, clinical psychologists, play specialists, occupational therapists and physiotherapists), along with fire fighters and adult burn survivors.</p>
        <p><strong>The Activities</strong><br/>The camps are carefully designed to help children and young people face the challenges of living with a burn injury. These may include:</p>
        <ul>
            <li><strong>Physically challenging activities</strong> such as rock climbing, abseiling and caving to build self-confidence in what their bodies can still achieve.</li>
            <li><strong>Day trips</strong> such as bowling and swimming which allow children to face the public with the support of each other.</li>
            <li><strong>Teamwork</strong> challenges to help improve social and communication skills.</li>
            <li>Small group activities to learn effective <strong>coping strategies</strong> for dealing with difficult situations such as teasing or bullying.</li>
        </ul>
    </div>

	<div id="text-whatwedo" class="paper content">
		<h2>What we do</h2>
		<p>Each year, around 750 <strong>burn injured children</strong> and young people are treated under the care of the Manchester Paediatric Burns Service. Those aged between 4 and 16 are invited to attend the appropriate camps and activities.</p>
		<p><strong>The Team</strong><br/>We are a team of specially trained <strong>volunteers</strong> who come from a variety of different health service professions (such as doctors, nurses, clinical psychologists, play specialists, occupational therapists and physiotherapists), along with fire fighters and adult burn survivors.</p>
		<p><strong>The Activities</strong><br/>The camps are carefully designed to help children and young people face the challenges of living with a burn injury. These may include:</p>
		<ul>
			<li><strong>Physically challenging activities</strong> such as rock climbing, abseiling and caving to build self-confidence in what their bodies can still achieve.</li>
			<li><strong>Day trips</strong> such as bowling and swimming which allow children to face the public with the support of each other.</li>
			<li><strong>Teamwork</strong> challenges to help improve social and communication skills.</li>
			<li>Small group activities to learn effective <strong>coping strategies</strong> for dealing with difficult situations such as teasing or bullying.</li>
		</ul>
	</div>
	<div id="text-volunteers" class="paper content">
		<a id="volunteer-left"></a>
		<div id="volunteer-content">
			<h2>Our Volunteers</h2>
			<div class="current">
				<img src="img/volunteer-frame.png" class="volunteer-img" style="background-image: url('img/volunteer-ian-melville.jpg');" alt="Ian Melville" />
				<p><span>Name: Ian Melville</span></p>
				<p><span>Age: 45</span></p>
				<p><span>From: Manchester</span></p>
				<p>Ian has been an operational Fire Fighter for 18 years, currently serving as a Watch Manager at Moss Side, Manchester.</p>
			</div>
			<div style="display: none;">
				<img src="img/volunteer-frame.png" class="volunteer-img" style="background-image: url('img/volunteer-michaela.jpg');" alt="Michaela" />
				<p><span>Name: Michaela</span></p>
				<p><span>From: Manchester</span></p><br>
				<p>Michaela is a nurse on the Burns Unit at the Royal Manchester Children's Hospital. She has volunteered on the younger children's Burns Camp for over 4 years.</p>
			</div>
      <div style="display: none;">
        <img src="img/volunteer-frame.png" class="volunteer-img" style="background-image: url('img/volunteer-michaela.jpg');" alt="Michaela" />
        <p><span>Name: Michaela</span></p>
        <p><span>From: Manchester</span></p><br>
        <p>Michaela is a nurse on the Burns Unit at the Royal Manchester Children's Hospital. She has volunteered on the younger children's Burns Camp for over 4 years.</p>
      </div>
		</div>
		<a id="volunteer-right"></a>
	</div>
	<div id="text-help" class="paper content">
		<h2>How you can help...</h2>
		<div id="help-content">
			<div id="help-p1">
				<p class="large-text"><a class="help-page" href="#">Next Page &gt;</a></p>
<p>It costs around £500 for a child to attend one of our residential Camps. All the camp
staff give their time voluntarily so we only pay for the facilities, equipment and for
qualified instructors to ensure safety at all times.</p>
<p><strong>Personal Donations</strong></p>
<p>We depend on charity donations – whether from a business, fundraising or a
personal contribution. Children who attend are not asked to contribute financially; but
a lot of the families who know how amazing our camps are have held a fundraising
event to say ‘thank you’ and ensure that places at the camp are available for others in
the future.</p>
<p><strong>Business Support</strong></p>
<p>If you’re looking for a local charity to support where you can see directly where
the money has gone, we are ideal. You know that 100% of your donation directly
provides a life-changing experience.</p>
<p>Each year we have a special Visitors Day at the Camps so you can come along and
see for yourself how your donation has been used.</p>
			</div>
			<div id="help-p2">
				<p class="large-text"><a class="help-page" href="#">&lt; Previous Page</a></p>
				<p><strong>You can download a copy of our Annual Report <a href="/files/annual-report-2016.pdf">here</a>.</strong></p>
				<div id="help-nav">
					<a target="_BLANK" href="http://www.justgiving.com/mcbf/Donate/" id="help-jg"></a>
					<a target="_BLANK" href="http://www.britannia.co.uk/_site/corporate/contact-us/branch-finder.html" id="help-brit"></a>
					<a href="#" id="help-voda"></a>
					<div style="clear: both;"></div>
					<p><strong>Click on a logo for instructions of how to donate.</strong></p>
				</div>
				<div class="help-t" id="help-jg-t">
					<p><strong>Clickety click...</strong></p>
					<p>The easiest and quickest way to donate to the Foundation is via our Just Giving page, just double click the logo and fill in the details on our page. It will take you 2 minutes and you can then get back to your brew!</p>
				</div>
				<div class="help-t" id="help-brit-t">
					<p><strong>Walk into a bank!</strong></p>
					<!--<p>If you would prefer to make a donation by cheque or cash, please make cheques payable to Children's Burns Foundation then print off and complete our paying in slip (http://www.cbf-uk.org/files/CBF_Paying_In_Slip.pdf). Then you can go into an actual bank (Co-operative Bank or Britannia Building Society) to pay it in. Click the logo for the branch locations.</p>--><p>Cheques payable to Manchester Children’s Burns Camp can be sent to the Burns Camp Co-Ordinator at:<br></br>
Burns Service,<br>
Royal Manchester Children’s Hospital,<br>
Oxford Rd,<br>
Manchester,<br>
M13 9WL</p>
				</div>
				<div class="help-t" id="help-voda-t">
					<p><strong>Join the mobile revolution...</strong></p>
					<p>Justgiving and Vodafone have joined together to allow charity supporters to donate easily by text message.
					Send a text to 70070 with the code CBFN00 followed by the amount you wish to donate. For example, "CBFN00 £5". You can donate up to £10 this way.  All the money raised goes straight to the charity, as Justgiving have waived their charges and for most networks, text messages are free. If you donate this way, you will receive a text back with a link to a Gift Aid form.</p>
				</div>
			</div>
		</div>
	</div>
    <img data-stellar-ratio="1.4" src="img/hotairballoon.png" id="sign-events" class="sign" alt="Events" />
    <img data-stellar-ratio="1.4" src="img/sign-whatwedo.png" id="sign-whatwedo" class="sign" alt="What we do" />
	<img data-stellar-ratio="1.4" src="img/sign-whowe.png" id="sign-whowe" class="sign" alt="Who we do it for" />
	<img data-stellar-ratio="1.3" src="img/big-tree.png" id="big-tree" alt="Big Tree" />
	<img data-stellar-ratio="1.1" src="img/log-n-lantern.png" id="log-lantern" alt="Log &amp; Lantern" />
	<img data-stellar-ratio="1.2" src="img/cabin.png" id="cabin" alt="Log Cabin" />
	<div data-stellar-ratio="1.1" id="phonebox">
		<div id="text-contact" class="paper">
			<div class="contact-right contact-right-2">
				<p>
					<strong><span>Tel:</span> 0161 701 8142<br/>
					<span>Mobile:</span> 07827 848240<br/>
					<!--<span>Enq:</span>--><br /> <a href="mailto:Alison.Thomlinson@cmft.nhs.uk">Alison.Thomlinson@cmft.nhs.uk</a></strong><br/><br/>
					Children's Burns Camp is part of the Central Manchester University Hospitals NHS Foundation Trust Charity (Registered Charity Number 1049274).
				</p>
			</div>
			<div class="contact-right">
				<p>
					<strong>Manchester Children's<br/>
					Burns Camp<br/>
					Burns Service<br/>
					Royal Manchester<br/>
					Children's Hospital<br/>
					Oxford Road<br/>
					Manchester<br/>
					M13 9WL</strong>
				</p>
			</div>
		</div>
	</div>
	<div data-stellar-ratio="1.1" id="sign-contact" class="sign"></div>
	<img data-stellar-ratio="0.7" src="img/hikers.png" id="hikers" alt="Hikers" />
	<img data-stellar-ratio="1.3" src="img/horses.png" id="horses" alt="Horses" />
	<img data-stellar-ratio="1.1" src="img/sign-volunteers.png" id="sign-volunteers" class="sign" alt="Our volunteers" />
	<img data-stellar-ratio="1.4" src="img/sign-help.png" id="sign-help" class="sign" alt="How you can help" />

	<img data-stellar-ratio="1.1" src="img/birds.png" id="birds" alt="Birds" />

	<div id="mountain-lake"></div>
	<div id="climber"></div>
	<div id="text-whowe" class="paper content">
		<h2>Who we do it for...</h2>
		<p><strong>Recovering From A Burn Injury</strong><br/>
Following a traumatic injury, children and young people can face many physical and
psychological challenges. The <strong>process of recovery</strong> can take a long time – arduous,
painful dressing changes and repeated trips to the hospital for further surgery are
combined with regular massage regimes and wearing specialist tight-fitting garments
for up to three years to help minimise scarring. They also have to adapt to their bodies
now looking different, learning to reintegrate into social and family life in a society
that often discriminates against disfigured people.</p>
<p><strong>How Burns Camps Help</strong><br/>
At Camp, they meet and socialise with young people just like themselves, as well as
meeting adult burn survivors and members of the Burn Care Team away from clinical
procedures.</p>
<p>So whether they are <strong>climbing</strong> a cliff face, <strong>canoeing</strong> across a lake, solving a <strong>team
challenge</strong>, going <strong>swimming</strong> for the first time since their injury or <strong>chatting</strong> late into
the night, these young people gain far more than a fun summer holiday. They gain
confidence and new skills to help them in the years ahead; they gain <strong>friendship and
support</strong> with others who totally understand.</p>
	</div>
</div>
<div id="sky"></div>

<div id="grass" data-stellar-background-ratio="1.5"></div>
	<script type="text/javascript" src="js/stellar.min.js"></script>

<?php if(!$mobile_device) { ?>
	<script type="text/javascript" src="js/func.js"></script>
<?php }else{ ?>
	<script type="text/javascript" src="js/portable.js"></script>
<?php } ?>
</body>
</html>
